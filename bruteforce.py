from itertools import combinations
import timeit


def most_profitable_stocks(data):
	start = timeit.default_timer()  # Timeur temps exécution code
	profit = []

	output = sum([list(map(list, combinations(data, i)))
				  for i in range(len(data) + 1)], []) # Parcourt de data afin degénérer une liste contenant toutes les combinaisons d'actions possibles

	for i in output: # une liste contenant plusieurs listes de combinaisons d'actions possibles
		if sum(i) <= 500: # Vérifie si la somme des valeurs d'une combinaison est inférieur ou égale à 500
			for el in i: # fait une itération sur chaque élement d'une combinaison
				a = (data[el][0] * el) / 100 # calcul de profit à partir du pourcentage 
				profit[-1].append((a, data[el][1])) # Rajout de la valeur du profit et du nom de l'action à chaque fois dans une nouvelle liste
			profit.append([])
	best_profit = {}

	for e in profit: # calcul de la somme des profits pour obtenir le profit total de chaque combinaison
		best_profit[sum((i[0] for i in e))] = [i[1] for i in e]

	
	print(f'Best Profit: {round(max(best_profit), 2)}\nStock names: {",".join(best_profit[max(best_profit)])} \n') # récupère le meilleur profit avec max()

	stop = timeit.default_timer()
	execution_time = stop - start

	print(f'Program Executed in {execution_time} ')


data = {20: (5, "Action-1"),
		30: (10, "Action-2"),
		50: (15, "Action-3"),
		70: (20, "Action-4"),
		60: (17, "Action-5"),
		80: (25, "Action-6"),
		22: (7, "Action-7"),
		26: (11, "Action-8"),
		48: (13, "Action-9"),
		34: (27, "Action-10"),
		42: (17, "Action-11"),
		110: (9, "Action-12"),
		38: (23, "Action-13"),
		14: (1, "Action-14"),
		18: (3, "Action-15"),
		8:  (8, "Action-16"),
		4: (12, "Action-17"),
		10: (14, "Action-18"),
		24: (21, "Action-19"),
		114: (18, "Action-20")
		}

most_profitable_stocks(data)
