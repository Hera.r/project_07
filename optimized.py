import csv
import timeit

def optimized_profit(data):
	start = timeit.default_timer()
	new_data = {}
	with open(data) as csv_file:
		csv_reader = csv.reader(csv_file, delimiter=',') # récupère les données du fichier csv
		for index, row in enumerate(csv_reader): # parcours la data pour stocker les données dans un dico sous la forme "profit en %" : ("coût", 'nom')	
			if index != 0:
				new_data[float(row[2])] = (float(row[1]), row[0])
	
	data_decroissant = sorted(new_data.keys(), reverse=True) # Tri des données par valeur dans l'ordre décroissant 
	compteur = 0
	result = {}
	total = 0

	for i in data_decroissant: # On parcourt les données triées pour récupérer les meilleurs valeurs tant qu'on ne dépasse pas un coût de 500
		if (compteur + new_data[i][0]) <= 500  and new_data[i][0] >= 0:
			# print(new_data[i][1])
			compteur += new_data[i][0]
			result[i] = new_data[i]

	print(f'################################################')
	print(f'Cost: {compteur}')
	
	for e in result: # calcul du profit total
		total += (e * result[e][0])/100

	print(f'Best Profit: {round(total, 2)}')
	stop = timeit.default_timer()
	execution_time = stop - start
	print(f'Program Executed in {round(execution_time, 2)} ')


optimized_profit("dataset1_Python+P7.csv")
print("##########################################")
optimized_profit("dataset2_Python+P7.csv")

