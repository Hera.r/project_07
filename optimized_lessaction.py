import timeit

def optimized_profit(data):
    start = timeit.default_timer()
    data_decroissant = sorted(data.keys(), reverse=True)
    compteur = 0
    result = {}
    total = 0
    for i in data_decroissant:
        if (compteur + data[i][0]) <= 500:
            compteur += data[i][0]
            result[i] = data[i]

    for e in result:
        total += (e * result[e][0])/100
    print(total)
    stop = timeit.default_timer()
    execution_time = stop - start
    print(f'Program Executed in {round(execution_time,2)} ')


data = {5: (20, "Action-1"),
        10: (30, "Action-2"),
        15: (50, "Action-3"),
        20: (70, "Action-4"),
        17: (60, "Action-5"),
        25: (80, "Action-6"),
        7: (22, "Action-7"),
        11: (26, "Action-8"),
        13: (48, "Action-9"),
        27: (34, "Action-10"),
        17: (42, "Action-11"),
        9: (110, "Action-12"),
        23: (38, "Action-13"),
        1: (14, "Action-14"),
        3: (18, "Action-15"),
        8:  (8, "Action-16"),
        12: (4, "Action-17"),
        14: (10, "Action-18"),
        21: (24, "Action-19"),
        18: (114, "Action-20")
        }
optimized_profit(data)